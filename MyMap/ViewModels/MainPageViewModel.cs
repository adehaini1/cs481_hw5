﻿using MyMap.Custom;
using MyMap.Model;
using MyMap.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MyMap.ViewModels
{  /// <summary>
   /// This is a viewmodel for mainpage.
   /// which is bind with mainpage through bindingcontext.
   /// </summary>
	public class MainPageViewModel : INotifyPropertyChanged
	{


		public MainPageViewModel()
		{   //ItemSource For Picker
			AddressLists = PickerServices.PinAddresses().ToList();
			Lang = AddressLists[0].Langitude;
			Long = AddressLists[0].Longitude;
			MapLabel = AddressLists[0].Label;
			MapAddress = AddressLists[0].Address;
			getdata();
		}
			 public Command StreetCommand
				{
					get
					{
						return new Command( () =>
						 {
							 customMap.MapType = MapType.Street;
						 });
					}
				}
		public Command SattelliteCommand
		{
			get
			{
				return new Command(() =>
				{
					customMap.MapType = MapType.Satellite;
				});
			}
		}
		public Command HybridCommand
		{
			get
			{
				return new Command(() =>
				{
					customMap.MapType = MapType.Hybrid;
				});
			}
		}

		void getdata()
		{
			
			customMap = new CustomMap();
			CustomPin pin = new CustomPin
			{
				Type = PinType.SearchResult,			
				Position = new Position(Lang, Long),
				Label = MapLabel,
				Address = MapAddress,
			};
		
			customMap.CustomPins = new List<CustomPin> {pin};
			customMap.Pins.Add(pin);
			customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(Lang, Long), Distance.FromMiles(0.5)));
		}
		
		string _label;
		public string MapLabel { get { return _label; } set { _label = value; OnPropertyChanged(); } }
		string _address;
		public string MapAddress { get { return _address; } set { _address = value; OnPropertyChanged(); } }

		Double _lang;
		public Double Lang { get { return _lang; } set { _lang = value; OnPropertyChanged(); } }
		Double _long;
		public Double Long { get { return _long; } set { _long = value; OnPropertyChanged(); } }
		CustomMap _custommap;
		public CustomMap customMap { get { return _custommap; } set { _custommap = value; OnPropertyChanged(); } }
		private List<CustomPin> _addresslist;
		public List<CustomPin> AddressLists { get { return _addresslist; } set { _addresslist = value; OnPropertyChanged(); } }

		private CustomPin _selectedAddress;
		public CustomPin SelectedAddress{get{return _selectedAddress;} 
			set{//This is fires when picker item is selected.
				_selectedAddress = value;
				MapLabel = _selectedAddress.Label.ToString();
				MapAddress= _selectedAddress.Address.ToString();
				Lang = _selectedAddress.Langitude;
				Long = _selectedAddress.Longitude;
				getdata();
				OnPropertyChanged();
			}}
		

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
	 [CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}
	}
}
