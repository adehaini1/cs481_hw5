﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace MyMap.Model
{/// <summary>
/// This is model properties for Map
/// </summary>
	public class CustomPin : Pin
    {
       
      
        public Double Langitude { get; set; }
        public Double Longitude { get; set; }
    }
}

