﻿using MyMap.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace MyMap.Custom
{/// <summary>
/// This is custom map model
/// </summary>
	public class CustomMap : Map
    {
        public List<CustomPin> CustomPins { get; set; }
    }

}
