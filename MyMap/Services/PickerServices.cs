﻿using MyMap.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace MyMap.Services
{/// <summary>
/// Address and location static data File
/// </summary>
	public class PickerServices: Pin
	{
		public static List<CustomPin> PinAddresses()
		{
			var address = new List<CustomPin>()
			 {   
				new CustomPin() { Address="11880 carmel mountain road san diego 92128", Label="In n out", Langitude=32.981600 , Longitude=-117.077580},
			    new CustomPin() { Address="12202 poway rd poway 92064", Label="Starbucks", Langitude=32.951210 , Longitude=-117.067570},
		    	 new CustomPin() { Address="11738 carmel mountain road san diego 92128", Label="Jamba juice", Langitude=32.982300 , Longitude=-117.078980},
    			  new CustomPin() { Address="272 e via rancho pjwy Escondido 92025", Label="West field north country", Langitude=33.072598 , Longitude=-117.067627}
			 };
			return address;			 
		}
	}
}
